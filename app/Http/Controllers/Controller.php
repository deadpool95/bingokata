<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function headers($request, $array = [], $status = 200, $json = TRUE) { //will return a json response with default data
        $date = Carbon::now('America/Lima');
        $headers = ['user_id' => $request->user_id ?? 'user', 'user_token' => $request->user_token ?? 'token', 'status' => 200, 'message' => NULL];
        $values = array_merge($headers, $array);
        if ($json === TRUE):
            return response()->json($values, $status);
        endif;
        return $values;
    }
}
