<?php

namespace App\Http\Controllers\api\v1\Room;

use App\BingoCard;
use App\CardNumber;
use App\Room;
use App\RoomNumber;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class RoomController extends Controller
{
    public function store(Request $request)
    { // generates the game room
        try {
            DB::connection('mysql')->beginTransaction();
            //generating the unique code for the room
            $maxroom = Room::max('code');
            if (!empty($maxroom)):
                $code = str_pad($maxroom + 1, 8, '0', STR_PAD_LEFT);
            else:
                $code = '00000001';
            endif;
            //generating the room
            $room = new Room(['code' => $code]);
            $room->save();
            $data = ['backend_response_code' => '0000', 'room' => $room->code, 'message' => 'Room successfully created', 'status' => 200];
            DB::connection('mysql')->commit();
        } catch (Exception $exception) {
            DB::connection('mysql')->rollBack();
            $data = ['backend_response_code' => '0001', 'message' => 'An error occurred', 'status' => 422];
        }
        return $this->headers($request, $data, $data['status']);
    }

    public function callNumber(Request $request)
    { //Calls a number for the game room
        $parameters = $request->all();
        try {
            //verifying if the room exists
            $room = Room::Filter(['Code' => $parameters['Code']])->select()->first();
            if (!empty($room)):
                for ($count = 1; $count <= 75; $count++) { // the rooms generated will be unique and only 75
                    $number = rand(1, 75); // generating a number from 1 to 75
                    //verifying if the generated number already exists on the list of numbers of the room
                    $room_number = RoomNumber::Filter(['Number' => $number, 'Room' => $room->id])->select()->first();
                    if (empty($room_number)):
                        //if not, proceed to save it
                        $room_number = new RoomNumber(['room_id' => $room->id, 'number' => $number]);
                        $room_number->save();
                        break;
                    endif;
                }
                //verifying that a number was generated
                if (!empty($room_number)):
                    //checking if the bingo cards of the room contains the generated number
                    $check = $this->checkNumberCalled($room_number->number, $room->id);
                    // if checking is OK, will show a successfully message
                    if ($check === 'OK'):
                        $data = ['backend_response_code' => '0000', 'number' => $room_number->number, 'check' => $check, 'message' => 'Number successfully generated', 'status' => 200];
                        return $this->headers($request, $data, $data['status']);
                    endif;
                    //if checking is not ok, it will show a warning message
                    $data = ['backend_response_code' => '0000', 'number' => $room_number->number, 'check' => $check, 'message' => 'Number generated, but not checked', 'status' => 422];
                    return $this->headers($request, $data, $data['status']);
                endif;
                $data = ['backend_response_code' => '0000', 'number' => '0', 'message' => 'No more numbers to generate', 'status' => 200];
                return $this->headers($request, $data, $data['status']);
            endif;
            $data = ['backend_response_code' => '0001', 'message' => 'Room does not exist.', 'status' => 422];
        } catch (Exception $exception) {
            DB::connection('mysql')->rollBack();
            $message = $exception->getMessage();
        }
        return $this->headers($request, $data, $data['status']);
    }

    public function checkNumberCalled($number, $room_id)
    { //Checks if the number called is on any card of the room
        try {
            //verifying if the room exists
            $room = Room::find($room_id);
            if (!empty($room)):
                // verifying if there are bingo cards in the room
                $cards = BingoCard::Filter(['Room' => $room->id, 'State'])->select()->get();
                if (!empty($cards)):
                    //for each card of the room it will verify if it contains the generated number
                    foreach ($cards as $card) {
                        //checking if the card contains the generated number
                        $card_number = CardNumber::Filter(['Number' => $number, 'Card' => $card->id])->select()->first();
                        if (!empty($card_number)):
                            //if it does, state is changed to 1
                            DB::connection('mysql')->beginTransaction();
                            $card_number->fill(['state' => 1])->save();
                            DB::connection('mysql')->commit();

                        endif;
                    }
                    $message = 'OK';
                else:
                    $message = 'No cards in room';
                endif;
            else:
                $message = 'Room does not exist';
            endif;
        } catch (Exception $exception) {
            DB::connection('mysql')->rollBack();
            $message = $exception->getMessage();
        }
        return $message;
    }
}
