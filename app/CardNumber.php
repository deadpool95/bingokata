<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CardNumber extends Model
{
    protected $connection = 'mysql';
    protected $table = 'card_numbers';
    protected $fillable = ['state','card_id', 'position', 'number'];
    protected $guarded = [];
    public $timestamps = false;

    public function scopeFilter($query, $items){
        $items = array_filter($items);
        if (count($items) > 0) :
            $items = (object)$items;
            $query->where(function ($query) use ($items) {
                if (!empty($items->Card)):
                    $query->where(['card_id' => $items->Card]);
                endif;
                if (!empty($items->Number)):
                    $query->where(['number' => $items->Number]);
                endif;
            });
        endif;
        return $query;
    }
}
