<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $table = 'rooms';
    protected $primaryKey = 'id';
    protected $fillable = ['code', 'state'];
    public $timestamps = true;

    public function scopeFilter($query, $items){
        $items = array_filter($items);
        if (count($items) > 0) :
            $items = (object)$items;
            $query->where(function ($query) use ($items) {
                if (!empty($items->Code)):
                    $query->where(['code' => $items->Code]);
                endif;
                if (!empty($items->State)):
                    $query->where(['state' => $items->Code]);
                endif;
            });
        endif;
        return $query;
    }
}
