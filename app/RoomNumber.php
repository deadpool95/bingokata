<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomNumber extends Model
{
    protected $connection = 'mysql';
    protected $table = 'room_numbers';
    protected $fillable = ['room_id', 'number'];
    protected $guarded = [];
    public $timestamps = false;

    public function scopeFilter($query, $items){
        $items = array_filter($items);
        if (count($items) > 0) :
            $items = (object)$items;
            $query->where(function ($query) use ($items) {
                if (!empty($items->Room)):
                    $query->where(['room_id' => $items->Room]);
                endif;
                if (!empty($items->Number)):
                    $query->where(['number' => $items->Number]);
                endif;
            });
        endif;
        return $query;
    }
}
