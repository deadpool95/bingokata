<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BingoCard extends Model
{
    protected $connection = 'mysql';
    protected $table = 'cards';
    protected $primaryKey = 'id';
    protected $fillable = ['user_id', 'room_id', 'code', 'state'];
    protected $guarded = [];
    public $timestamps = false;

    public function scopeFilter($query, $items){
        $items = array_filter($items);
        if (count($items) > 0) :
            $items = (object)$items;
            $query->where(function ($query) use ($items) {
                if (!empty($items->Room)):
                    $query->where(['room_id' => $items->Room]);
                endif;
                if (!empty($items->User)):
                    $query->where(['user_id' => $items->User]);
                endif;
                if (!empty($items->State)):
                    $query->where(['state' => $items->State]);
                endif;
                if (!empty($items->Code)):
                    $query->where(['code' => $items->Code]);
                endif;
            });
        endif;
        return $query;
    }
}
