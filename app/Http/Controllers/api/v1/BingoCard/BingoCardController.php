<?php

namespace App\Http\Controllers\api\v1\BingoCard;

use App\BingoCard;
use App\CardNumber;
use App\Room;
use App\RoomNumber;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Exception;

class BingoCardController extends Controller
{
    public function generate(Request $request) // function for generating a bingo card
    {
        $parameters = $request->all();
        try {
            //validating if the room exists
            $room = Room::Filter(['Code' => $parameters['Code']])->select()->first();
            if (!empty($room)):
                //validating if there is a existing bingo card for the user and room given
                $card = BingoCard::Filter(['Room' => $room->id, 'User' => $parameters['user_id']])->select()->first();
                if (empty($card)):
                    //generating the unique code for the bingo card
                    $maxCard = BingoCard::Filter(['Room' => $room->id])->select()->max('code');
                    if (!empty($maxCard)):
                        $code = str_pad($maxCard + 1, 5, '0', STR_PAD_LEFT);
                    else:
                        $code = '00001';
                    endif;
                    //generating the bingo card
                    DB::connection('mysql')->beginTransaction();
                    $card = new BingoCard(['user_id' => $parameters['user_id'], 'room_id' => $room->id, 'code' => $code, 'state' => 0]);
                    $card->save();
                    //generating the numbers of the bingo card
                    for ($count = 1; $count <= 25; $count++) { // 25 is the maximum quantity of numbers per bingo card
                        if ($count != 13): // position 13 of the bingo card will be always empty
                            $success = 0;
                            while ($success === 0) {
                                $number = rand(1, 75); // generating numbers from 1 to 75
                                //verifying if there is already the number generated on the bingo card
                                $card_number = CardNumber::Filter(['Number' => $number, 'Card' => $card->id])->select()->first();
                                if (empty($card_number)):
                                    //if not, proceed to generate the number for the bingo card
                                    $card_number = new CardNumber(['card_id' => $card->id, 'number' => $number, 'position' => $count, 'state' => 0]);
                                    $card_number->save();
                                    break;
                                endif;
                            }
                        endif;
                    }
                    DB::connection('mysql')->commit();
                    $data = ['backend_response_code' => '0000', 'bingo_card' => $card, 'message' => 'Bingo Card successfully generated', 'status' => 200];
                    return $this->headers($request, $data, $data['status']);
                endif;
                $data = ['backend_response_code' => '0000', 'message' => 'User already has a bingo card.', 'status' => 422];
                return $this->headers($request, $data, $data['status']);
            endif;
            $data = ['backend_response_code' => '0001', 'message' => 'Room does not exist.', 'status' => 422];
        } catch (Exception $exception) {
            DB::connection('mysql')->rollBack();
            $data = ['backend_response_code' => '0001', 'message' => 'An error occurred', 'status' => 422];
        }
        return $this->headers($request, $data, $data['status']);
    }

    public function check(Request $request) // function for checking if the bingo card is completed
    {
        $parameters = $request->all();
        try {
            DB::connection('mysql')->beginTransaction();
            //verifying if the bingo card exists
            $card = BingoCard::Filter(['Code' => $parameters['bingo_code']])->select()->first();
            if (!empty($card)):
                //listing the numbers of the bingo card given
                $card_numbers = CardNumber::Filter(['Card' => $card->id])->select()->get();
                $count = 0; //this var will count the number of matching numbers
                foreach ($card_numbers as $card_number) {
                    // checking if the number is listed on the room's numbers list
                    $check_room_number = RoomNumber::Filter(['Room' => $card->room_id, 'Number' => $card_number->number])->select()->first();
                    if (!empty($check_room_number)):
                        $count++; // if it does, will plus 1 to the count var
                    endif;
                }
                //updating the matching numbers amount to the card
                $card->fill(['state' => $count]);
                $card->save();
                if ($count === 24): // if it has 24 matching numbers, the card is the winner
                    $data = ['backend_response_code' => '0000', 'message' => 'You win!', 'status' => 200];
                    return $this->headers($request, $data, $data['status']);
                endif;
                $yet = 24 - $count; //calculating the amount of matching numbers to win
                $data = ['backend_response_code' => '0000', 'message' => 'Your card is not completed yet! ' . $yet . ' numbers to win!', 'status' => 200];
                return $this->headers($request, $data, $data['status']);
            endif;
            DB::connection('mysql')->commit();
            $data = ['backend_response_code' => '0001', 'message' => 'Card does not exist.', 'status' => 422];
            return $this->headers($request, $data, $data['status']);

        } catch (Exception $exception) {
            DB::connection('mysql')->rollBack();
            $data = ['backend_response_code' => '0001', 'message' => 'An error occurred', 'status' => 422];
        }
        return $this->headers($request, $data, $data['status']);
    }
}
